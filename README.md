# Rock Paper Scissors Spock Lizard #

Programmed by David Doughty

Models by Eric Eckhouse

Uses the [Leap Motion Controller](www.leapmotion.com) to implement the
 [Rock Paper Scissors Spock Lizard](www.samkass.com/theories/RPSSL.html) rules invented by Sam Kass and Karen Bryla 

Download at http://asleepypenguin.itch.io/rock-paper-scissors-spock-lizard

### This is a work in progress. ###

**This repository is not the complete project:** There are assets imported from the Unity Asset Store that are not included in this repository.